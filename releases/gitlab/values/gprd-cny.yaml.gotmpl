---

# Please note that `gprd.yaml.gotmpl` is included first for the `gprd-cny` environment,
# this file is for `cny` specific overrides

nginx-ingress:
  common:
    labels:
      stage: cny
  controller:
    autoscaling:
      minReplicas: 3

gitlab:
  gitlab-pages:
    gitlabCache:
      expiry: 30m
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-pages@{{ .Values.google_project }}.iam.gserviceaccount.com
  gitlab-shell:
    minReplicas: 8
    extraEnv:
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
    metrics:
      enabled: true
    sshDaemon: gitlab-sshd
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    service:
      # gcloud compute address ssh-gke-gprd-cny
      loadBalancerIP: 10.216.8.61
  mailroom:
    enabled: false
    image:
      repository: {{ .Environment.Values.registry_path }}/gitlab-mailroom
      # Pin the tag to avoid following the chart default
      # https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/201
      tag: 0.0.19

  sidekiq:
    enabled: false
  webservice:
    common:
      labels:
        shard: default
        stage: cny
        tier: sv
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"cny\"}"
        hpa:
          # keeping minReplicas high enough to not suffer from node scaling
          # during deployments (https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592)
          minReplicas: 16
        service:
          # gcloud compute address api-gke-gprd-cny
          loadBalancerIP: 10.216.8.13
      git:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-git
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
        service:
          # gcloud compute address git-https-gke-gprd-cny
          loadBalancerIP: 10.216.8.10
      internal-api:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"internal-api\", \"stage\": \"cny\"}"
      web:
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"web\", \"stage\": \"cny\"}"
          CANARY: "true"
          MALLOC_CONF: "narenas:2"
        hpa:
          minReplicas: 16
          maxReplicas: 50
        service:
          # gcloud compute address web-gke-gprd-cny
          loadBalancerIP: 10.216.8.87
      websockets:
        hpa:
          minReplicas: 3
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-websockets
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"websockets\", \"stage\": \"cny\"}"
          GODEBUG: madvdontneed=1
        service:
          # gcloud compute address websockets-gke-gprd-cny
          loadBalancerIP: 10.216.8.15
    minReplicas: 5
    extraEnv:
      ENABLE_RBTRACE: 1
      GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"git\", \"stage\": \"cny\"}"
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
      GITLAB_LOG_DEPRECATIONS: "true"
      GITLAB_DIAGNOSTIC_REPORTS_ENABLED: "true"
      prometheus_rust_multiprocess_metrics: "true"
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-cny-webservice@{{ .Values.google_project }}.iam.gserviceaccount.com
  kas:
    service:
      # gcloud compute address kas-internal-gke-gprd-cny
      loadBalancerIP: 10.216.8.85

global:
  appConfig:
    contentSecurityPolicy:
      enabled: true
      report_only: false
      directives:
        connect_src: "<default_value>"
        frame_ancestors: "<default_value>"
        frame_src: "<default_value>"
        img_src: "<default_value>"
        object_src: "<default_value>"
        script_src: "<default_value>"
        style_src: "<default_value>"
        worker_src: "<default_value>"
        report_uri: "https://sentry.gitlab.net/api/105/security/?sentry_key=a42ea3adc19140d9a6424906e12fba86&sentry_environment=gprd-cny"

  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-gprd-cny
      - 10.216.8.25
    externalHttps:
      # gcloud compute address pages-gke-gprd-cny
      - 10.216.8.25
registry:
  hpa:
    minReplicas: 5
  service:
    # gcloud compute address registry-gke-gprd-cny
    loadBalancerIP: 10.216.8.18
  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: gitlab-cny-registry@{{ .Values.google_project }}.iam.gserviceaccount.com
  # When enabled, the upload purger will attempt to make deletes against the
  # common GCS bucket. It is not suitable to be enabled in production until the
  # following issues are resolved:
  # https://gitlab.com/gitlab-org/container-registry/-/issues/216
  # https://gitlab.com/gitlab-org/container-registry/-/issues/217
  maintenance:
    uploadpurging:
      enabled: false
  gc:
    disabled: false

gitlab-zoekt:
  install: false
